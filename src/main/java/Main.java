import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class Main {

    private static String CHARSET_NAME = "utf8";

    public static void main(String[] args) {
        String resourcePath = args[0];
        String resourcePath1 = args[1];
        String targetElementId = args[2];


        Optional<Element> buttonElement = findElementById(new File(resourcePath), targetElementId);
        if (buttonElement.isPresent()) {
            List<Element> allElements = getAllElements(new File(resourcePath1));

            Optional<WithMatchingRate<Element>> bestMatchingElement = allElements
                    .stream().map(e -> new WithMatchingRate<>(computeMatchingRateForElements(buttonElement.get(), e), e))
                    .max(Comparator.comparing(e -> e.matchingRate));

            if (bestMatchingElement.isPresent()) {
                System.out.println("Best matching element: " + bestMatchingElement.get().element.toString());
                System.out.println("Best matching element's path: " + toStringPath(bestMatchingElement.get().element));
            }


        } else {
            System.out.println("There is not element with id " + targetElementId);
        }

    }

    private static String toStringPath(Element e) {
        return e.parents()
                .stream().map(Main::elementToPathNode)
                .reduce((e1, e2) -> e2 + " > " + e1).orElse("") + " > " + elementToPathNode(e);
    }

    private static String elementToPathNode(Element element) {
        Element parent = element.parent();
        if (parent != null && parent.children().stream().filter(e -> e.tagName().equals(element.tagName())).count() > 1)
            return element.tagName() + "[" + element.elementSiblingIndex() + "]";
        return element.tagName();
    }

    private static Map<String, String> attributesMap(Element element) {
        return element.attributes().asList().stream().collect(Collectors.toMap(Attribute::getKey, Attribute::getValue));
    }

    private static int computeMatchingRateForElements(Element e1, Element e2) {
        int matchingRate = 0;
        matchingRate += e1.tagName().equals(e2.tagName()) ? 1 : 0;
        matchingRate += computeMatchingRateForAttributes(attributesMap(e1), attributesMap(e2));
        return matchingRate;
    }

    private static int computeMatchingRateForAttributes(Map<String, String> m1, Map<String, String> m2) {
        return m1.entrySet().stream().mapToInt(e -> {
            int matches = 0;
            String value = m2.get(e.getKey());
            matches += value != null ? 1 : 0;
            matches += value != null && value.equals(e.getValue()) ? 1 : 0;
            return matches;
        }).sum();
    }

    private static Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.ofNullable(doc.getElementById(targetElementId));

        } catch (IOException e) {
            System.out.println("Error reading file");
            e.printStackTrace();
            return Optional.empty();
        }
    }

    private static List<Element> getAllElements(File htmlFile) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());
            return new ArrayList<>(doc.getAllElements());
        } catch (IOException e) {
            System.out.println("Error reading file");
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    private static class WithMatchingRate<T> {
        private int matchingRate;
        private T element;

        WithMatchingRate(int matchingRate, T element) {
            this.matchingRate = matchingRate;
            this.element = element;
        }
    }

}
